<?php

require_once dirname(__FILE__).'/../lib/member_infoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/member_infoGeneratorHelper.class.php';

/**
 * member_info actions.
 *
 * @package    sensingtechnology
 * @subpackage member_info
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class member_infoActions extends autoMember_infoActions
{
   public function executeNew(sfWebRequest $request)
  {
    $this->form = $this->configuration->getForm();
    $this->member_info = $this->form->getObject();
  }
}
