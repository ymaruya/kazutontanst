<?php

/**
 * auth actions.
 *
 * @package    fcreator
 * @subpackage auth
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class authActions extends sfActions
{


 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward($this->getModuleName(), 'input');
  }


  /**
   * Executes input action
   *
   * @param sfWebRequest $request
   * @return sfView
   */
  public function executeInput(sfWebRequest $request)
  {
    $this->form = new myLoginForm();
    $this->form->getWidgetSchema()->setNameFormat(myLoginForm::NAME.'[%s]');
    if ($request->isMethod('post'))
    {
      $values = $request->getParameter($this->form->getName());
      $this->form->bind($values);
      if ($this->form->isValid()) {
        $memberInfo = $this->form->getMemberInfo();
        $this->form->save();
        $this->login($memberInfo);
      }
    }
  }


  /**
   * Executes logout action
   *
   * @param sfWebRequest $request
   */
  public function executeLogout(sfWebRequest $request)
  {
    $this->getUser()->doLogout();
    $this->redirect("@homepage");
  }


  /**
   * login
   * ログイン処理と、その後のリダイレクト
   *
   * @param MemberInfo $user
   * @param bool $isset_remember_key
   */
  private function login(MemberInfo $user)
  {
    $this->getUser()->doLogin($user);
    $this->redirect("top/menu");
  }
}
