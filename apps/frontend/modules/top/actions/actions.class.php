<?php

/**
 * top actions.
 *
 * @package    sensingtechnology
 * @subpackage top
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class topActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  
  public function executeMenu(sfWebRequest $request)
  {
      $this->member_info = $this->getUser()->getMemberInfo();  
      $this->gps_receive_interval =  sfConfig::get('app_gps_receive_interval');  
  }
  
  public function executeLogout(sfWebRequest $request)
  {
      $this->getUser()->doLogout();
      $this->redirect("top/menu");
  }
}
