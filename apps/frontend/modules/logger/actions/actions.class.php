<?php

/**
 * logger actions.
 *
 * @package    sensingtechnology
 * @subpackage logger
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class loggerActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->forward('default', 'module');
    }

    public function executeInput(sfWebRequest $request) {

        //requestパラメータから値を取得
        $id = $request->getParameter("id");
        $member_info = MemberInfoTable::getInstance()->findOneById($id);
        $this->forward404Unless($member_info, '無効なIDです');

        //getParameterの引数に、GPSAndAccelerationRecever.jsで指定したid
        $receive_status = $request->getParameter("receive_status");

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        $this->getResponse()->setHttpHeader('Cache-Control', 'no-cache, must-revalidate');

        if (($receive_status == 0) || ($receive_status == 1)) {
            //if文の中でデータを保存
            $member_log = new MemberLog();
            $member_log->setMemberInfoId($id);
            $member_log->setLatitude($request->getParameter("lat"));
            $member_log->setLongitude($request->getParameter("lng"));
            $is_init = ((string) $request->getParameter("is_init") == "true") ? true : false;
            $member_log->setReceiveStatus($receive_status);
            $xy = ConvertToxy::getInstance()->execute($request->getParameter("lat"), $request->getParameter("lng"));
            $member_log->setX($xy["x"]);
            $member_log->setY($xy["y"]);
            //$v = $request->getParameter("v"); //m/sec 
            //if((string)$v === "NaN") $v = 0; 
            //$member_log->setV($v);
            //初回は無視
            if (!$is_init){
                $tmp = MemberLogTable::getInstance()->getMyV($id, $xy["x"], $xy["y"],$request->getParameter("gps_time"));
                $v = $tmp["v"];
                $dis = $tmp["dis"];
            }
            else {
                $v = -1;
                $dis = -1;
            }
            $member_log->setV($v);
            $member_log->setDistance($dis);
            $member_log->setAx($request->getParameter("x"));
            $member_log->setAy($request->getParameter("y"));
            $member_log->setAz($request->getParameter("z"));

            $member_log->setGpsTime($request->getParameter("gps_time"));
            $member_log->save();
            return $this->renderText(json_encode(array("v" => $v,"dis" => $dis)));
        }
        else
            return $this->renderText(json_encode(array("v" => -1,"dis" => -1)));
    }

}
