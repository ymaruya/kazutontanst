<?php use_stylesheet("frontend/outputlog/list.css") ?> 
<table class="tb">
    <caption class="tbcc">ログ確認ページ *自分のみ</caption>
    <tr><th>アクセス日時</th><th>緯度</th><th>経度</th></tr>
    <?php if ($pager): ?>
        <?php foreach ($pager->getResults() as $member_log): ?>
            <tr><td><?php echo $member_log->getCreatedAt(); ?></td><td><?php echo $member_log->getLatitude(); ?></td><td><?php echo $member_log->getLongitude(); ?></td></tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>

<?php if ($pager): ?>

    <?php echo link_to("CSVで出力する", $sf_context->getModuleName() . "/dlList?is_all=0"); ?>
    <?php //echo link_to("すべてのユーザログをCSVで出力する", $sf_context->getModuleName() . "/dlList?is_all=1"); ?>
    <br /><br />
<?php endif; ?>
<div class="pager">
    <?php if ($pager): ?>    
        <?php foreach ($pager->getLinks() as $page): ?>
            <?php if ($page == $pager->getPage()): ?>
                <?php echo $page ?>
            <?php else: ?>
                <a href="<?php echo url_for('outputlog/list?page=' . $page); ?>"><?php echo $page ?></a>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<!-- //structre -->

<!-- InstanceEndEditable --> 
<!-- /#container --> 
</div>
