<?php use_stylesheet("frontend/outputlog/googlemap.css") ?> 
<?php use_javascript("http://maps.google.com/maps/api/js?v=3&sensor=false"); ?>


<body onload="init()">
    <script type="text/javascript">
        function attachMessage(marker) {
            google.maps.event.addListener(marker, 'click', function() {
                new google.maps.Geocoder().geocode({
                    latLng: marker.getPosition()
                }, function(result, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        new google.maps.InfoWindow({
                            content: marker.getTitle()
                        }).open(marker.getMap(), marker);
                    }
                });
            });
        }
        //<![CDATA[
        var map;
        // 初期化。bodyのonloadでinit()を指定することで呼び出してます
        function init() {
            var latlngs = new Array();//マーカー位置の緯度経度
            var names = new Array();
            var times = new Array();
            var v = new Array(); //速度
            var x = new Array();


//google map apiをデータを直す
<?php foreach ($member_logs as $member_log): ?>
                latlngs.push(new google.maps.LatLng(<?php echo $member_log->getLatitude() ?>, <?php echo $member_log->getLongitude() ?>));
                names.push("<?php echo MemberInfoTable::getInstance()->findOneById($member_log->getMemberInfoId())->getUsername(); ?>");
                times.push("<?php echo $member_log->getCreatedAt(); ?>");
                v.push("<?php echo $member_log->getV(); ?>");
<?php endforeach; ?>
    

            //初期設定
            var opts = {
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(35.729757,139.71056) //中心をどこにするか
            };

            // getElementById("map")の"map"は、body内の<div id="map">より
           //気にしないで
            map = new google.maps.Map(document.getElementById("map"), opts);
          
            //マーカの設置
            //どんなユーザが存在しているかは気にしないので、色で速度を分ける。
            for (i = 0; i < latlngs.length; i++) {
                //色を指定
                var color = '|'+makeRGB(v[i]) + '|000000'; //|背景色|RGB
                
                //マーカーを作成
                var myMarker = new google.maps.Marker({
                    position: latlngs[i], //どこに表示するか
                    map: map, //気にしないで
                    title: " 時間 : " + times[i] + " 速度:" +v[i], //ポップアップの部分
                    icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=i'+ color //icon
                });
              
                //マーカーをセット
                attachMessage(myMarker);
                //google.maps.event.addListener(myMarker, 'click', clickEventFunc);
            }
        }
        
        //RGBで速度を表現する
        //http://www.kesion.com/xd/rgb.htm
        /*stop : color = 191970(MidnightBlue), v<2.1
         *walk : color = 8470FF(LightSlateBlue),2.0 < v <5.1
         *run : color = 7CFC00(LawnGreen), 5.0 < v < 10.1
         *bike : color = FFFF00(Yellow), 10.0 < v < 21.0
         *car : color = B8860B(DarkGoldenrod), 20.0 < v < 61.1
         *oher : color = FF0000(Red)
         */
        function makeRGB(v){
            var color;
            if(v < 2.1) color = '191970';
            else if((2.0 < v) && (v < 5.1)) color = '8470FF';
            else if((5.0 < v) && (v < 10.1)) color = '7CFC00';
            else if((10.0 < v) &&(v < 21.1)) color = 'FFFF00';
            else if((20.0 < v) && (v < 61.1)) color = 'B8860B';
            else color = 'FF0000';
            return color
        }

        function clickEventFunc(event) {
            alert(event.latLng.toString());
        }

        //]]>
    </script>
    <div id="map"></div>
</body>