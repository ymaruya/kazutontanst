
<div id="content" class="structre">
  <div class="section">
      <h1>検索画面(Googlemap APIの制約上,GoogleMapの最大表示数200件,CSVは無制限)</h1>
    <?php if($form->hasErrors()): ?>
    <div class="errorBox clearfix">
      <ul>
        <?php
             $labels = $form->getWidgetSchema()->getLabels();
             foreach ($form->getErrorSchema()->getNamedErrors() as $key => $err) {
               $label = isset($labels[$key]) ? $labels[$key].": " : "";
               echo "<li>" . $label . $err . "</li>\n";
             }
             foreach ($form->getGlobalErrors() as $key => $err) {
               echo "<li>" . $err . "</li>\n";
             }
        ?>
      </ul>
      <!-- / .errorBox clearfix -->
    </div>
    <?php endif; ?>
    
   <?php echo $form->renderFormTag(url_for('outputlog/search')); ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th>検索ユーザー</th>
        <td><?php echo $form['is_all']; ?></td>
      </tr>
      <tr>
        <th>
          現在時刻かどうか
        </th>
        <td>
          <?php echo $form['time_option']; ?>
        </td>
      </tr>
      <tr>
        <th>
          時刻指定
        </th>
        <td>
              <?php echo $form['datetime']; ?>
        </td>
      </tr>
      <tr>
        <th>
          検索時間範囲(分)
        </th>
        <td>
          <?php echo $form['datetime_ranege']; ?>
        </td>
      </tr>
      <tr>
        <th>
          検索件数
        </th>
        <td>
              <?php echo $form['datetime_optopn']; ?>
        </td>
      </tr>
      <tr>
        <th>
          出力媒体
        </th>
        <td>
              <?php echo $form['output']; ?>
        </td>
      </tr>
    </table>

    <input type="hidden" name="submit" value="検索" />
    <div class="btnRegist">
      <input name="place" type="submit" value="検索" />
      <!-- / .btnRegist -->
    </div>
  </form>
    <!-- //section -->
  </div>
  <!-- / #content -->
</div>
<!-- /#container -->
