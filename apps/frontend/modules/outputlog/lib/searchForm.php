<?php

class searchForm extends sfForm {

    const NAME = 'google_map_info';

    public function configure() {
        // -----------
        //   widgets
        // -----------
        $c1 = array('0' => '自分のみ', '1' => 'すべてのユーザ');
        $c2 = array('0' => '現在時刻', '1' => '時刻を指定');
        $c3 = array('0' => 'すべて','1' => '10', '2' => '20','3' => '30');
        $c4 = array( '0' => 'すべて','1' => '最新の1件のみ');
        $c5 = array( '0' => 'googlemapで見る','1' => 'csvで出力する');

        $this->setWidgets(array(
            'is_all' => new sfWidgetFormChoice(array(
                'expanded' => false,
                'choices' => $c1
            )),
            'time_option' => new sfWidgetFormChoice(array(
                'expanded' => false,
                'choices' => $c2
            )),
            
            'datetime' => new sfWidgetFormDateTime(array(
                
            )),
                    
            'datetime_ranege' => new sfWidgetFormChoice(array(
                'expanded' => false,
                'choices' => $c3
            )),
            
            'datetime_optopn' => new sfWidgetFormChoice(array(
                'expanded' => false,
                'choices' => $c4
            )),
            'output' => new sfWidgetFormChoice(array(
                'expanded' => false,
                'choices' => $c5
            ))
        ));
           

        $this->widgetSchema->setLabels(array(
                'is_all' => '対称ユーザー',
                'time_option' => '時刻指定',
                'datetime' => "日時",
                'datetime_ranege' => "範囲",
                'datetime_optopn' => "オプション",
                'output' => '出力媒体'
            ));

      
        // --------------
        //   validators
        // --------------
        // columns
        $this->setValidators(array(
            'is_all' => new sfValidatorBoolean(),
            'time_option' => new sfValidatorBoolean(),
            'datetime' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => "Y-m-d H:i")),
            'datetime_ranege' =>  new sfValidatorPass(),
            'datetime_optopn' => new sfValidatorPass(),
            'output' => new sfValidatorPass()
        ));


         $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'glovalValidator')))
        );
        // ----------
        //   others
        // ----------
         $this->validatorSchema->setOption('allow_extra_fields', true); 
         $this->validatorSchema->setOption('filter_extra_fields', false); 
         $this->disableLocalCSRFProtection();
    }
    
    public function glovalValidator($validator, $values){
   
        if($values["time_option"] ){
            if(!$values["datetime"]) throw new sfValidatorError($validator, '日時を指定してください。');
        }
        else $values["datetime"] = date("Y-m-s H:i");
        $values["datetime_ranege"] *=10;
        
        $is_all = $values["is_all"];
        $output = $values["output"];

        if($is_all and $output) {
            throw new sfValidatorError($validator,'すべてのユーザーのログは出力で来ません。');
        }
        
        return $values;
    }
    
}

