<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * do login check
 *
 * @author jarapeno
 */
class myLoginCheckFilter extends sfFilter
{
  /**
   * execute
   *
   * @param <type> $filterChain
   */
  public function execute($filterChain)
  {	
    $this->getContext()->getUser()->LoginCheck();
    $filterChain->execute();
  }
}