<?php

/*緯度・経度から平面直角座標x,yおよび子午線収差角を求める計算*/
//参考: http://vldb.gsi.go.jp/sokuchi/surveycalc/algorithm/

class ConvertToxy{
    
    private static $instance = null;
     
    private $e1 = 0.08181919104281579; //第一離心率
    private $e2 = 0.08209443815191719; //第二離心率
    private $a = 6378137; //長半径
    private $f = 0.003352810681182319; //篇平率
    private $m0 = 0.9999;
    
    private function __construct() {
    }

    
    public static function getInstance(){
         if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    
    /**
     *
     * @param type $lattidute
     * @param type $longtitude
     * 
     * return xy
     */
    public function execute($lat,$lon){
        //vars
        $lat = $this->DEG_TO_RAD($lat);
        $lon = $this->DEG_TO_RAD($lon);
        
        $ram0 = array("lat" => $this->DEG_TO_RAD(36.0) , "lon" => $this->DEG_TO_RAD(139.83)); //λ0
        
        $S0 = $this->_MeridianArcLength($ram0["lat"]); //S0
        $S = $this->_MeridianArcLength($lat); //S1
        $N = $this->getN($lat);
        $M = $this->getM($lat);
       
        return array("x" => $this->convert_x($ram0["lat"], $ram0["lon"], $lat, $lon, $S0, $S, $N),"y" => $this->convert_y($ram0["lat"], $ram0["lon"], $lat, $lon, $S0, $S, $N));
    }
    
     //緯度を与えて赤道からの子午線弧長を求める計算
    /**
     *
     * @param type $latitude
     * @return S
     */
    private function _MeridianArcLength($latitude){
        $B1 = 6335439.327083875;
        $B2 = -0.9567422112495888;
        return $B1 * $latitude + $B2 * sin(2 * $latitude);
    }
    
    
    private function getN($lat){
        return $this->a / $this->getW($lat);
    }
    
    private function getM($lat){
        return $this->a * (1 - pow($this->e1,2)) / pow($this->getW($lat),3);
    }
    
    private function getW($lat){
        return sqrt(1 - pow($this->e1,2) * pow(sin($lat),2));
    }
    
    private function convert_x($lat0,$lon0,$lat,$lon,$s0,$s,$n){
        $t = tan($lat);
        $d_ram = $lon - $lon0;
        $p2 = $this->e2 * pow(cos($lat),2);
        
        $section1 = $s - $s0;
        $section2 = 1/2 * $n * pow(cos($lat),2) * $t * pow($d_ram,2);
        $section3 = 1/24 *$n * pow(cos($lat),4) * $t * (5 - pow($t,2) + 9 * $p2 + 4 * pow($p2,2)) * pow($d_ram,4); 
        $section4 = -1 * 1/720 * $n * pow(cos($lat),6) * $t * (320 * pow($t,2) * $p2 - 270 * $p2 - pow($t,4) + 58 * pow($t,2) -61) * pow($d_ram,6);
        $section5 = -1 * 1/40320 * $n * pow(cos($lat),8) * $t * (pow($t,6) - 543 * pow($t,4) + 3111 * pow($t,2) -1385) * pow($d_ram,8);
        return ($section1 + $section2 + $section3 + $section4 + $section5) * $this->m0;
    }
    
    private function convert_y($lat0,$lon0,$lat,$lon,$s0,$s,$n){
        $t = tan($lat);
        $d_ram = $lon - $lon0;
        $p2 = $this->e2 * pow(cos($lat),2);
        
        $section1 = $n * cos($lat) * $d_ram;
        $section2 = -1 * 1/6 * $n * pow(cos($lat),3) * (-1 + pow($t,2) -1 * $p2) * pow($d_ram,3);
        $section3 = -1 * 1/120 * $n * pow(cos($lat),5) * (-5 + 18 * pow($t,2) -1 * pow($t,4) -14 * $p2 + 58 * pow($t,2) * $p2) * pow($d_ram,5);
        $section4 = -1 * 1/5040 * $n * pow(cos($lat),7) * (-61 + 479 * pow($t,2) -179 * pow($t,4) + pow($t,6)) * pow($d_ram,7);
        return ($section1 + $section2 + $section3 + $section4) * $this->m0;
    }
    
    private function DEG_TO_RAD($p){
        return $p * pi() / 180;
    }
}
?>
