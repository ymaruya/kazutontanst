<?php

/**
 * MemberInfo form.
 *
 * @package    sensingtechnology
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MemberInfoForm extends BaseMemberInfoForm
{
  public function configure()
  {
       unset($this['created_at'], $this['updated_at'],$this["deleted_at"]);
  }
}
